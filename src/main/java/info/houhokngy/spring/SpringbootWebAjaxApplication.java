package info.houhokngy.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootWebAjaxApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootWebAjaxApplication.class, args);
	}
}
